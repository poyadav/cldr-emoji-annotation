#!/bin/sh

for dir in annotations annotationsDerived; do
    for xml in /usr/share/unicode/cldr/common/$dir/*.xml; do
        echo xmllint --noout --valid --postvalid $xml
        xmllint --noout --valid --postvalid $xml
        if [ $? -ne 0 ] ; then
            echo "ERROR"
            exit 1
        fi
    done
done

